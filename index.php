<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="Colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>PHP</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		/* Show it is fixed to the top */
		body {
		  min-height: 75rem;
		  padding-top: 5.5rem;
		}
	</style>
</head>

<?php include "config/koneksi.php"; 
	$regist = mysqli_query($con, "SELECT * FROM bemrec");
?>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <a class="navbar-brand" href="#">BRAND</a>
	  </div>
	</nav>

	<div class="container"> 
	    <div class="row" >

			<div class="col-md-4">
				<div class="card">
			  		<div class="card-header">Input</div>
		  			<form method="post" action="save_guest.php" role="form">
				  	<div class="card-body">
						<div class="form-group">
							<label for="nama">Nama*</label>
							<input type="text" class="form-control" id="name" name="nama" placeholder="Name" required>
							<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
						</div>
						<div class="form-group">
							<label for="gender">Gender</label>
							<input type="text" class="form-control" id="gender" name="gender" placeholder="jenis kelamin">
						</div>
						<div class="form-group">
							<label for="email">E-mail</label>
							<input type="text" class="form-control" id="email" name="email" placeholder="e-mailmu apa nak">
						</div>
						<div class="form-group">
							<label for="jurusan">Jurusan</label>
							<input type="text" class="form-control" id="Jurusan" name="jurusan" placeholder="jurusan yang diambil">
						</div>
						<div class="form-group">
							<label for="angkatan">Angkatan</label>
							<input type="text" class="form-control" id="Angkatan" name="angkatan" placeholder="angkatan keberapa contoh 05">
						</div>
						<div>
			            	<label for="alamat">Alamat</label>
			            	<textarea class="form-control" type="textarea" id="Alamat" name="alamat" placeholder="Alamat tempat tinggal kamu" maxlength="140" rows="7"></textarea>
			                <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>          
			            </div>
			        </div>
				  	<div class="card-footer">
				  		<a href="javascript:location.reload(true)"  class="btn btn-light">Cancel</a>
				  		<button type="submit" id="submit" name="submit" class="btn btn-info ml-2">Submit</button>
				  	</div>

			  		</form>
				</div>
				
			</div>

				
			<div class="col-md-8">
				<div class="card">
				  <div class="card-header">
				  	List Users
				  </div>
				  <div class="card-body">
							
					 <table class="table table-striped">
					    <thead>
					      <tr>
					        <th>nama</th>
					        <th>gender</th>
				            <th>email</th>
				            <th>jurusan</th>
				            <th>angkatan</th>
				            <th>alamat</th>
				            <th>action (crud)</th>
					      </tr>
					    </thead>
					    <tbody>
				        	<?php while ($g = mysqli_fetch_array($regist)){ ?>
					            <tr>
					                <td width="10%"><?php echo $g['nama'] ?></td>
					                <td width="10%"><?php echo $g['gender'] ?></td>
					                <td width="10%"><?php echo $g['email'] ?></td>
					                <td width="10%"><?php echo $g['jurusan'] ?></td>
					                <td width="10%"><?php echo $g['angkatan'] ?></td>
					                <td width="10%"><?php echo $g['alamat'] ?></td>
					                <td><button type="button" class="btn btn-secondary btn-sm" style="margin-right: 4px" data-toggle="modal" data-target="#edit-<?php echo $g['id'] ?>"><i class="glyphicon glyphicon-pencil"></i> Edit </button>

					                	<a href="delete_guest.php?user=<?php echo $g['id'] ?>"  class="btn btn-danger btn-sm" onclick="return confirm('Apakah user ini akan di hapus ?');"><i class="glyphicon glyphicon-trash"></i> Del </a>
					         
					            	</td>
					            </tr>
								<!-- Trigger the modal with a button -->
								<!-- Modal -->
								<div id="edit-<?php echo $g['id']; ?>" class="modal" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <!-- Modal Header -->
								      <div class="modal-header">
								        <h4 class="modal-title">Edit</h4>
								        <button type="button" class="close" data-dismiss="modal">&times;</button>
								      </div>

								      <form  method="post" action="save_edit_guest.php?user=<?php echo $g['id'] ?>" role="form">
									      <div class="modal-body">
									      	<div class="row">
									      		<div class="col-md-12">
								    				<div class="form-group">
								    					<label> Nama </label>
														<input type="text" class="form-control" id="name" name="nama" placeholder="Nama" value="<?php echo $g['nama'] ?>" required>
														<span class="help-block"><p class="help-block ">*wajib diisi</p></span>
													</div>
													<div class="form-group">
								    					<label> Gender </label>
														<input type="text" class="form-control" id="mobile" name="gender" placeholder="08xxx" value="<?php echo $g['gender'] ?>">
													</div>
													<div class="form-group">
								    					<label> Email </label>
														<input type="text" class="form-control" id="mobile" name="email" placeholder="08xxx" value="<?php echo $g['email'] ?>">
													</div>								            
								                    <div class="form-group">
								    					<label> Jurusan </label>
														<input type="text" class="form-control" id="mobile" name="jurusan" placeholder="Jurusan yang diambil" value="<?php echo $g['jurusan'] ?>">
													</div>
													<div class="form-group">
								    					<label> Angkatan </label>
														<input type="text" class="form-control" id="mobile" name="angkatan" placeholder="Jurusan yang diambil" value="<?php echo $g['angkatan'] ?>">
													</div>
													<div class="form-group">
								    					<label> Alamat </label>
								                    	<textarea class="form-control" type="textarea" id="message_edit" name="alamat" placeholder="Alamat" maxlength="140" rows="7"> <?php echo $g['alamat'] ?> </textarea>        
								                    </div>
										    	</div>
										    </div>
									      </div>
									      <div class="modal-footer">
									        <button type="submit" name="submit" class="btn btn-primary pull-right">Save</button>
									      </div>
								      </form>
								  </tbody>
								</table>
								    </div>
								  </div>
								</div></tbody></table></div></div></div></div></div></body></html>
				        	<?php } ?>
				  </div> 
				</div>
			</div>

		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#characterLeft').text('140 characters left');
		    $('#addr').keydown(function () {
		        var max = 140;
		        var len = $(this).val().length;
		        if (len >= max) {
		            $('#characterLeft').text('You have reached the limit');
		            $('#characterLeft').addClass('red');
		            $('#btnSubmit').addClass('disabled');            
		        } 
		        else {
		            var ch = max - len;
		            $('#characterLeft').text(ch + ' characters left');
		            $('#btnSubmit').removeClass('disabled');
		            $('#characterLeft').removeClass('red');            
		        }
		    });  

		} );
	</script>

</body>
</html>

